import os


def setenv_gcp():
    env_name = 'GOOGLE_APPLICATION_CREDENTIALS'
    if env_name not in os.environ or env_name in os.environ and 'secret' not in os.environ[env_name]:
        os.environ[env_name] = "mlops-jmp-15bcc365febe.json"
    return

