import os
import random
import logging
import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.flask import FlaskIntegration
import numpy as np
import tensorflow as tf

"""
    Initialisation du package
"""
__APPLICATION_NAME__ = __package__
__version__ = "0.9.0"

"""
    Logging config 
"""

# All of this is already happening by default!
sentry_logging = LoggingIntegration(
    level=logging.INFO,  # Capture info and above as breadcrumbs
    event_level=logging.ERROR  # Send errors as events
)
sentry_sdk.init(
    dsn="https://5fc18665b41a4622a3c36c2bd331a5e3@o563583.ingest.sentry.io/5753717",
    integrations=[sentry_logging, FlaskIntegration()]
)

# Set the random seeds (source wandb tutorail https://colab.research.google.com/github/wandb/examples/blob/master/\
# colabs/keras/Simple_Keras_Integration.ipynb#scrollTo=9-APP7-6C5uK)
os.environ['TF_CUDNN_DETERMINISTIC'] = '1'
random.seed(hash("setting random seeds") % 2 ** 32 - 1)
np.random.seed(hash("improves reproducibility") % 2 ** 32 - 1)
tf.random.set_seed(hash("by removing stochasticity") % 2 ** 32 - 1)
