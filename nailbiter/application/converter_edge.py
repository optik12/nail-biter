import os
import json
import subprocess
import re
import sys
import tensorflow as tf
import tensorflowjs as tfjs
from tensorflow.python.compiler.tensorrt import trt_convert as trt
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.preprocessing import LabelBinarizer
from imutils import paths
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

import logging

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger(__name__)


# Convert to TensorRT for Jetson Nano, Xavier NX...
def export_tensorrt(savedmodel_tf_format_dir, export_trt_dir='export_models/trt'):
    """
    Export a TF model (>2.3) to a TensorRT model.
    :param savedmodel_tf_format_dir: custom naming for this experiment
    :param export_trt_dir: name of the run
    """
    # create dir if not exist
    if not os.path.exists(export_trt_dir):
        os.makedirs(export_trt_dir)

    converter_trt = trt.TrtGraphConverterV2(input_saved_model_dir=savedmodel_tf_format_dir)
    converter_trt.convert()
    converter_trt.save(export_trt_dir)


# Convert to TFLite from TF model: usage for Mobile, Edge device
def converter_tf2tflite_unquantized(savedmodel_tf_format_dir='saved_models', input_modelname='unquantized',
                                    export_tflite_unq_dir='export_models/trt'):
    """
        Use the Tensorflow Converter python API
        Convert a TF2 model to TFlite as an a basic un-quantized TFlite model
    """
    converter_tflite_unquantized = tf.lite.TFLiteConverter.from_saved_model(savedmodel_tf_format_dir)
    tflite_model_tf = converter_tflite_unquantized.convert()

    # create dir if not exist
    if not os.path.exists('export_models/tflite'):
        os.makedirs('export_models/tflite')

    # tf_export_file
    tf_export_tflite_file = 'export_models/tflite/model_fromsavedmodel' + '_' + input_modelname + '.tflite'

    # Save the TF Lite model.
    with tf.io.gfile.GFile(tf_export_tflite_file, 'wb') as f:
        f.write(tflite_model_tf)

    _logger.info("[CONVERTER]:TF Model has been converted to unquantized TFLite model")
    # print(tf_export_tflite_file)


# Convert to TFLite from TF.keras h5 model: usage for Mobile, Edge device
def converter_h5tflite_unquantized(savedmodel_h5_format_dir='saved_models', input_modelname='unquantized',
                                   export_tflite_unq_dir='export_models/trt'):
    """
        Use the Tensorflow Converter python API
        Convert a Keras H5 model to TFlite as an a basic un-quantized TFlite model
    """
    # reload model from save h5*
    model = tf.keras.models.load_model(savedmodel_h5_format_dir)
    _logger.info("[CONVERTER]:TF.keras model reloaded")

    converter_from_h5_tflite_unquantized = tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model_h5 = converter_from_h5_tflite_unquantized.convert()

    # create dir if not exist
    if not os.path.exists('export_models/tflite'):
        os.makedirs('export_models/tflite')

    # tf_export_file
    h5_export_tflite_file = 'export_models/tflite/model_fromkeras_edge' + '_' + input_modelname + '.tflite'

    # Save the TF Lite model.
    with tf.io.gfile.GFile(h5_export_tflite_file, 'wb') as f:
        f.write(tflite_model_h5)

    _logger.info("[CONVERTER]:TF.Keras h5 Model has been converted to unquantized TFLite model")
    # print(tf_export_tflite_file)


# we need to reload dataset for Post training quantization
# locate image dataset collected in our working folder
dataset = '../../data/raw'

# grab the list of images in our dataset directory, then initialize
# the list of data (i.e., images) and class images
imagePaths = list(paths.list_images(dataset))
data = []
labels = []

# loop over the image paths
for imagePath in imagePaths:
    # extract the class label from the filename
    label = imagePath.split(os.path.sep)[-2]

    # load the input image (224x224) and preprocess it (input shape must be 224x224 for MobileNetV2)
    image = load_img(imagePath, target_size=(224, 224))
    image = img_to_array(image)
    image = preprocess_input(image)

    # update the data and labels lists, respectively
    data.append(image)
    labels.append(label)

# convert the data and labels to NumPy arrays
data = np.array(data, dtype="float32")
labels = np.array(labels)

# perform one-hot encoding on the labels
lb = LabelBinarizer()
labels = lb.fit_transform(labels)
labels = to_categorical(labels)

# partition the data into training and testing splits using 75% of
# the data for training and the remaining 25% for testing
test_size = 0.20
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=test_size, stratify=labels, random_state=12)


# Convert the model again with post-training quantization (TF2)
# A generator that provides a representative dataset and create TF-lite Flatbuffer
# More info on Readme about post-training quantization
def representative_data_gen():
    for _ in range(100):
        image = next(iter(trainX))
        # image = tf.io.read_file(image)
        # image = tf.io.decode_jpeg(image, channels=3)
        # image = tf.image.resize(image, [IMAGE_SIZE, IMAGE_SIZE])
        # image = tf.cast(image / 255., tf.float32)
        image = tf.expand_dims(image, 0)
        yield [image]


# reload model from save h5*
savedmodel_h5_format_dir = 'saved_models'
model = tf.keras.models.load_model(savedmodel_h5_format_dir)
converter = tf.lite.TFLiteConverter.from_keras_model(model)
# This enables quantization
converter.optimizations = [tf.lite.Optimize.DEFAULT]
# This sets the representative dataset for quantization
converter.representative_dataset = representative_data_gen
# This ensures that if any ops can't be quantized, the converter throws an error
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
# For full integer quantization, though supported types defaults to int8 only, we explicitly declare it for clarity.
converter.target_spec.supported_types = [tf.int8]
# These set the input and output tensors to uint8 (added in TF branch r2.3)
converter.inference_input_type = tf.uint8
converter.inference_output_type = tf.uint8
tflite_model = converter.convert()

# h5_export_file_quant
model_export_file_quant = 'export_models/tflite/model_quant' + '_' + 'input_saved_modelname' + '.tflite'

with open(model_export_file_quant, 'wb') as f:
    f.write(tflite_model)

    _logger.info(
        f"[CONVERTER]:TF.Keras h5 Model has been converted to quantized (post training quantization) TFLite model "
        f"{model_export_file_quant}")

# Display the accuracy on the test set prior to check with quantize model
# batch predict on the testing set
predictions = model.predict(testX, batch_size=32)

# display pretty classification report ;
print(classification_report(testY.argmax(axis=1),
                            predictions.argmax(axis=1), target_names=lb.classes_))
# _logger.info(
#         f"[CONVERTER]:classifiction report: {(classification_report(testY.argmax(axis=1),
#                             predictions.argmax(axis=1), target_names=lb.classes_))}")

# create a dummy iterator for conveniance on the test set; could be improve
augv = ImageDataGenerator()
val_generator = augv.flow(testX, testY, batch_size=32)

batch_images, batch_labels = next(val_generator)


def set_input_tensor(interpreter, input):
    input_details = interpreter.get_input_details()[0]
    tensor_index = input_details['index']
    input_tensor = interpreter.tensor(tensor_index)()[0]
    # Inputs for the TFLite model must be uint8, so we quantize our input data.
    # NOTE: This step is necessary only because we're receiving input data from
    # ImageDataGenerator, which rescaled all image data to float [0,1]. When using
    # bitmap inputs, they're already uint8 [0,255] so this can be replaced with:
    #   input_tensor[:, :] = input
    scale, zero_point = input_details['quantization']
    input_tensor[:, :] = np.uint8(input / scale + zero_point)


def classify_image(interpreter, input):
    set_input_tensor(interpreter, input)
    interpreter.invoke()
    output_details = interpreter.get_output_details()[0]
    output = interpreter.get_tensor(output_details['index'])
    # Outputs from the TFLite model are uint8, so we dequantize the results:
    scale, zero_point = output_details['quantization']
    output = scale * (output - zero_point)
    return np.argmax(output)


# feature in TF core 2.3: https://www.tensorflow.org/api_docs/python/tf/lite/Interpreter
interpreter = tf.lite.Interpreter(model_export_file_quant)
interpreter.allocate_tensors()

# Collect all inference predictions in a list
batch_prediction = []
batch_truth = np.argmax(batch_labels, axis=1)

for i in range(len(batch_images)):
    prediction = classify_image(interpreter, batch_images[i])
    batch_prediction.append(prediction)

# Compare all predictions to the ground truth
tflite_accuracy = tf.keras.metrics.Accuracy()
tflite_accuracy(batch_prediction, batch_truth)
print("Quant TF Lite accuracy: {:.4%}".format(tflite_accuracy.result()))

# TODO Compile for EDGE TPU
# https://coral.ai/docs/edgetpu/compiler/

"""
install TPU edge compiler
# https://coral.ai/docs/edgetpu/compiler/#download
# TPU runtime version and Compiler version have some mandatory requirements

curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
sudo apt-get update -y
sudo apt-get install -U edgetpu-compiler -y
"""

# print(pycoral.utils.edgetpu.get_runtime_version())"

# check TPU compiler version
bashCmd1 = ["edgetpu_compiler", "--version"]
process = subprocess.Popen(bashCmd1, stdout=subprocess.PIPE)
output, error = process.communicate()
# print(output)
_logger.info(f"[TPU_COMPILER]: {output}")

# check TPU compiler version
bashCmd0 = ["pwd"]
process = subprocess.Popen(bashCmd0, stdout=subprocess.PIPE)
output, error = process.communicate()
# print(output)
_logger.info(f"[TPU_COMPILER]: locate whereiam: {output}")

# create a quant folder if not exist
if not os.path.exists('export_models/quant_compile'):
    os.makedirs('export_models/quant_compile')

# get compiler args
# Runtime version on Target TPU is 13 (min is 10, max is 15 at this date)
compilerCmd = ["edgetpu_compiler", "-s", "export_models/tflite/model_quant_input_saved_modelname.tflite", "-o",
               "export_models/quant_compile", "--min_runtime_version", "13"]
process = subprocess.Popen(compilerCmd, stdout=subprocess.PIPE)
output, error = process.communicate()
# print(output)
_logger.info(f"[TPU_COMPILER]: {output}")

# save the class labels to a text file to put on the Edge device with the TF-lite quantized model
with open('export_models/quant_compile/labels.txt', 'w') as f:
    for i in range(len(lb.classes_)):
        print(lb.classes_.item(i))
        _logger.info(f"[TPU_COMPILER]: class labels are: {lb.classes_.item(i)}")
        f.write(lb.classes_.item(i) + '\n')

# Convert to TensorflowJS
# use tfjs converter https://github.com/tensorflow/tfjs & https://github.com/tensorflow/tfjs/tree/master/tfjs-converter

# check TPU compiler version
bashCmd2 = ["pwd"]
process = subprocess.Popen(bashCmd2, stdout=subprocess.PIPE)
output, error = process.communicate()
# print(output)
_logger.info(f"[TPU_COMPILER]: locate whereiam: {output}")

# check TPU compiler version symlink to TF 'saved_models' folder because converter use absolute path The desired
# output format. Must be tfjs_layers_model, tfjs_graph_model or keras. Not all pairs of input-output formats are
# supported. Please file a github issue if your desired input-output pair is not supported.
# only tfjs converter 2.0.1 give sucessuful results; model TF is tf.keras 2.4
bashCmd3 = ["tensorflowjs_converter", "--input_format=keras", "--output_format", "tfjs_layers_model",
            "model.h5", "./export_models/tfjs_c201"]
process = subprocess.Popen(bashCmd3, stdout=subprocess.PIPE)
output, error = process.communicate()
# print(output)
_logger.info(f"[TPU_COMPILER]: tfjs converted files location are in: export_models/tfjs_c201")

# For Netlify & ml5.js usage : append model.json previously converted with the ml5specs key-pairs values (class labels
# name) TODO read from labels.txt
ml5specs = {"ml5Specs": {"mapStringToIndex": ["nail-biting", "no-nail-biting"]}}

with open('export_models/tfjs_c201/model.json', 'r+') as file:
    data = json.load(file)
    data.update(ml5specs)
    file.seek(0)
    json.dump(data, file)

# ---
# # 'dirty' FIX for inconsistency between TF version and TFJS version and Netlify/Keras embeding
# # python replace is case sensitive
# with open('export_models/tfjs_c201/model.json', 'r+') as file:
#     d = json.load(file)
#     for k in d["modelTopology"]:
#         for l in d["modelTopology"]:
#
#     content = file.read()
#     file.seek(0)
#     # data = json.loads(json.dumps(file).replace('"Functional"', '"Model"'))
#     # content = file.read()
#     content.replace("Functional", "Model")
#     # '"class_name": "Model"'
#     #  file.seek(0)
#     # json.dump(data, file)
#     file.write(content)
#
#     # data = json.loads(
#     re.sub('Functional', 'Model')
#     file.seek(0)
#     json.dump(data, file)

# with open('export_models/tfjs_c201/model.json', 'r+') as file:
#     data = json.load(file)
#
# for item in data['modelTopology']:
#     item['class_name'] = item['class_name'].replace('Functional', 'Model')
#
# with open('new_data.json', 'w') as file:
#     json.dump(data, file)

# with open('export_models/tfjs_c201/model.json', 'r+') as file:
#     data = json.load(file)
#     for item in data:
#         if item['class_name'] in ["Functional"]:
#             item['class_name'] = "Model"
# with open('export_models/tfjs_c201/model.json', 'w') as file:
#     json.dump(data, file)
# ---

if __name__ == '__main__':
    export_tensorrt('saved_models')
    converter_tf2tflite_unquantized('saved_models')
    converter_h5tflite_unquantized('saved_models_checkpoint'
                                   '/weights_best_from_nailbiter_detector_20210510_232604_btchSz32_nEpch20_lr0.0001'
                                   '.h5')
