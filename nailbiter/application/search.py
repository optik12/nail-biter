# # search.py# # search.py
# import click
# import numpy as np
# from hyperopt import fmin, hp, tpe, rand
#
# import mlflow.projects
# from mlflow.tracking.client import MlflowClient
#
#
# @click.command(help="Hyperparameter search with Hyperopt.")
# @click.option("--max-runs", type=click.INT, default=10, help="Maximum number of runs.")
# @click.option("--epochs", type=click.INT, default=500, help="Number of train steps.")
# @click.option("--metric", type=click.STRING, default="rmse", help="Metric to optimize.")
# @click.option("--algo", type=click.STRING, default="tpe.suggest", help="Search algorithm.")
# @click.argument("data")
# def search(data, max_runs, epochs, metric, algo):
#     tracking_client = mlflow.tracking.MlflowClient()
#     # initial value for the parameter to be optimized
#     _inf = np.finfo(np.float64).max
#     # define the search space for hyper-parameters
#     space = [
#         hp.uniform('lr', 1e-5, 1e-1),
#     ]
#     with mlflow.start_run() as run:
#         exp_id = run.info.experiment_id
#         # run the optimization algorithm
#         best = fmin(
#             fn=train_fn(epochs, exp_id, _inf, _inf),
#             space=space,
#             algo=tpe.suggest if algo == "tpe.suggest" else rand.suggest,
#             max_evals=max_runs
#         )
#         mlflow.set_tag("best params", str(best))
#         # find all runs generated by this search
#         client = MlflowClient()
#         query = "tags.mlflow.parentRunId = '{run_id}' ".format(run_id=run.info.run_id)
#         runs = client.search_runs([exp_id], query)
#         # iterate over all runs to find best one
#         best_train, best_valid = _inf, _inf
#         best_run = None
#         for r in runs:
#             if r.data.metrics["val_rmse"] < best_val_valid:
#                 best_run = r
#                 best_train = r.data.metrics["train_rmse"]
#                 best_valid = r.data.metrics["val_rmse"]
#         # log best run metrics as the final metrics of this run.
#         mlflow.set_tag("best_run", best_run.info.run_id)
#         mlflow.log_metrics({
#             "train_{}".format(metric): best_train,
#             "val_{}".format(metric): best_valid
#         })
#
#
# def train_fn(epochs, null_train_loss, null_valid_loss):
#     ...
#
#
# if __name__ == '__main__':
#     search()

# import click
# import numpy as np
# from hyperopt import fmin, hp, tpe, rand
#
# import mlflow.projects
# from mlflow.tracking.client import MlflowClient
#
#
# @click.command(help="Hyperparameter search with Hyperopt.")
# @click.option("--max-runs", type=click.INT, default=10, help="Maximum number of runs.")
# @click.option("--epochs", type=click.INT, default=500, help="Number of train steps.")
# @click.option("--metric", type=click.STRING, default="rmse", help="Metric to optimize.")
# @click.option("--algo", type=click.STRING, default="tpe.suggest", help="Search algorithm.")
# @click.argument("data")
# def search(data, max_runs, epochs, metric, algo):
#     tracking_client = mlflow.tracking.MlflowClient()
#     # initial value for the parameter to be optimized
#     _inf = np.finfo(np.float64).max
#     # define the search space for hyper-parameters
#     space = [
#         hp.uniform('lr', 1e-5, 1e-1),
#     ]
#     with mlflow.start_run() as run:
#         exp_id = run.info.experiment_id
#         # run the optimization algorithm
#         best = fmin(
#             fn=train_fn(epochs, exp_id, _inf, _inf),
#             space=space,
#             algo=tpe.suggest if algo == "tpe.suggest" else rand.suggest,
#             max_evals=max_runs
#         )
#         mlflow.set_tag("best params", str(best))
#         # find all runs generated by this search
#         client = MlflowClient()
#         query = "tags.mlflow.parentRunId = '{run_id}' ".format(run_id=run.info.run_id)
#         runs = client.search_runs([exp_id], query)
#         # iterate over all runs to find best one
#         best_train, best_valid = _inf, _inf
#         best_run = None
#         for r in runs:
#             if r.data.metrics["val_rmse"] < best_val_valid:
#                 best_run = r
#                 best_train = r.data.metrics["train_rmse"]
#                 best_valid = r.data.metrics["val_rmse"]
#         # log best run metrics as the final metrics of this run.
#         mlflow.set_tag("best_run", best_run.info.run_id)
#         mlflow.log_metrics({
#             "train_{}".format(metric): best_train,
#             "val_{}".format(metric): best_valid
#         })
#
#
# def train_fn(epochs, null_train_loss, null_valid_loss):
#     ...
#
#
# if __name__ == '__main__':
#     search()
