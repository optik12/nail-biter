"""
Some utils helpers functions for MLFlow
"""

import logging
import time
import mlflow
from mlflow.tracking.client import MlflowClient
from mlflow.entities.model_registry.model_version_status import ModelVersionStatus

logger = logging.getLogger(__name__)


# cf tuto https://docs.databricks.com/_static/notebooks/mlflow/mlflow-model-registry-example.html
def wait_until_ready(model_name, model_version):
    client = MlflowClient()
    for _ in range(10):
        model_version_details = client.get_model_version(
            name=model_name,
            version=model_version,
        )
        status = ModelVersionStatus.from_string(model_version_details.status)
        print("Model status: %s" % ModelVersionStatus.to_string(status))
        if status == ModelVersionStatus.READY:
            break
        time.sleep(1)


def newest_model_version(mlflow_client, model_name):
    model_version_infos = mlflow_client.search_model_versions("name = '%s'" % model_name)
    return max(
        int(model_version_info.version)
        for model_version_info in model_version_infos
    )


def transition_newest_to_stage(mlflow_client, model_name="model", stage="Staging"):
    new_model_version = newest_model_version(mlflow_client, model_name)
    mlflow_client.transition_model_version_stage(model_name,
                                                 version=new_model_version,
                                                 stage=stage)
