import argparse
import os
import time
import random
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
# import tensorflowjs as tfjs
from sentry_sdk import capture_message
from google.cloud import storage
from imutils import paths
from pygit2 import Repository

from nailbiter.application.mlflow_utils import wait_until_ready, transition_newest_to_stage
from nailbiter.domain.utilsplot import utils  # credits to https://github.com/saurabh2mishra/mlflow-demo

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam, SGD, RMSprop, Adadelta
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import classification_report, plot_roc_curve, confusion_matrix, accuracy_score, precision_score, \
    roc_curve, auc
import seaborn as sns
from imutils import paths
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

from nailbiter.api.mlflowserving.settings import MLFLOW_TRACKING_URI

import mlflow
import mlflow.tensorflow
import mlflow.tracking
from mlflow.tracking import MlflowClient

import logging

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger(__name__)

if os.getenv('DB_USER') is None:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'mlops-jmp-15bcc365febe.json'
else:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = '/secrets/key.json'

# client = storage.Client()

_logger.info("[TRAINING]:Classe Train Initialization")
tf.keras.backend.clear_session()

# wandb reco https://colab.research.google.com/github/wandb/examples/blob/master/colabs/keras
# /Simple_Keras_Integration.ipynb#scrollTo=XmVkQbw_q_07
os.environ['TF_CUDNN_DETERMINISTIC'] = '1'
random.seed(hash("setting random seeds") % 2 ** 32 - 1)
np.random.seed(hash("improves reproducibility") % 2 ** 32 - 1)
tf.random.set_seed(hash("by removing stochasticity") % 2 ** 32 - 1)

# locate image dataset collected in our working folder
dataset = '../../data/raw'

# grab the list of images in our dataset directory, then initialize
# the list of data (i.e., images) and class images
imagePaths = list(paths.list_images(dataset))
data = []
labels = []

# loop over the image paths
for imagePath in imagePaths:
    # extract the class label from the filename
    label = imagePath.split(os.path.sep)[-2]

    # load the input image (224x224) and preprocess it (input shape must be 224x224 for MobileNetV2)
    image = load_img(imagePath, target_size=(224, 224))
    image = img_to_array(image)
    image = preprocess_input(image)

    # update the data and labels lists, respectively
    data.append(image)
    labels.append(label)

# convert the data and labels to NumPy arrays
data = np.array(data, dtype="float32")
labels = np.array(labels)

# perform one-hot encoding on the labels
lb = LabelBinarizer()
labels = lb.fit_transform(labels)
labels = to_categorical(labels)

# partition the data into training and testing splits using 75% of
# the data for training and the remaining 25% for testing
test_size = 0.20
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=test_size, stratify=labels, random_state=12)

# Collect feature size info
imgSize0 = len(trainX[0])
imgSize1 = len(trainX[0][0])
numPixels = imgSize0 * imgSize1
numTrainImages = len(trainX)

_logger.info("[TRAINING]:Training dataset has " + str(numTrainImages) + " images")
_logger.info("[TRAINING]:Testing dataset has " + str(len(testX)) + " images")

# get all training and test shape tensors
print(trainX.shape)
print(trainY.shape)
print(testX.shape)
print(testY.shape)

# # quick overview of the training dataset
# train_labels_count = np.unique(trainY, return_counts=True)
# dataframe_train_labels = pd.DataFrame({'Label':train_labels_count[0], 'Count':train_labels_count[1]})
#
# # quick overview of the testing dataset
# test_labels_count = np.unique(testY, return_counts=True)
# dataframe_test_labels = pd.DataFrame({'Label':test_labels_count[0], 'Count':test_labels_count[1]})

# initialize the initial learning rate, number of epochs to train for,
# and batch size

# Set experiment name; if one does exist, one will be created for you
# Use MlflowClient() to fetch the experiment details

# Enable auto-logging to MLflow
mlflow.tensorflow.autolog()

# client = MlflowClient()
# mlflow.set_experiment('MLflow nail biter alerter')
# entity = client.get_experiment_by_name('MLflow nail biter alerter')
# exp_id = entity.experiment_id

# epochs = 30
# batch_size = 32 # 64, 128, 256
# init_lr = 1e-4

parser = argparse.ArgumentParser(description='Train a Keras face mask detector in TensorFlow2/Keras')
parser.add_argument('--epochs', '-e', type=int, default=1)  # 5, 15, 25, 50, 20
parser.add_argument('--batch_size', '-b', type=int, default=32)
parser.add_argument('--init_lr', '-il', type=float, default=0.0001)
parser.add_argument('--learning_rate', '-l', type=float, default=0.05)
parser.add_argument('--psize', '-ps', type=int, default=7)
parser.add_argument('--num_hidden_units', '-n', type=int, default=128)
parser.add_argument('--dropout', '-d', type=float, default=0.50)
parser.add_argument('--momentum', '-m', type=float, default=0.85)
args = parser.parse_args()

# use iterator ImageDataGenerator for data augmentation; expose as war for mlflow logging
rotation_range = 20
zoom_range = 0.15
width_shift_range = 0.2
height_shift_range = 0.2
shear_range = 0.15
horizontal_flip = True
fill_mode = 'nearest'

aug = ImageDataGenerator(
    rotation_range=rotation_range,
    zoom_range=zoom_range,
    width_shift_range=width_shift_range,
    height_shift_range=height_shift_range,
    shear_range=shear_range,
    horizontal_flip=horizontal_flip,
    fill_mode=fill_mode)


def get_optimizer(opt_name):
    """
    :param opt_name: name of the Keras optimizer
    :return: Keras optimizer
    """

    if opt_name == 'Adam_decay':
        return Adam(lr=args.init_lr, decay=args.init_lr / args.epochs)

    elif opt_name == 'SGD':
        return keras.optimizers.SGD(lr=args.learning_rate,
                                    momentum=args.momentum,
                                    nesterov=True)
    elif opt_name == 'RMSprop':
        return RMSprop(lr=args.learning_rate, rho=0.9, epsilon=None, decay=0.0)

    else:
        return Adadelta(lr=args.learning_rate, epsilon=None, decay=0.0)


# some hints for tensorboard + some callbacks
# https://www.tensorflow.org/tensorboard/image_summaries
# ------------------------------------------------------------------------------------------------------
# Writer tensorboard
jobName = 'nailbiter_detector_{}_btchSz{}_nEpch{}_lr{}'.format(
    time.strftime("%Y%m%d_%H%M%S"), args.batch_size, args.epochs, args.init_lr)

# tensorboard_fp = os.path.join('runs', jobName)
logdir = os.path.join('runs', jobName)
# ------------------------------------------------------------------------------------------------------
print('launch tensorboard --logdir runs={0}` to visualize results on tensorboard \n'
      .format(logdir))
# ---------------------------------------------------------------------------------------------------------------
# callbacks

if not os.path.exists('saved_models'):
    os.makedirs('saved_models')

# add ModelCheckpoint callback to record best weight only
checkpointer = ModelCheckpoint(monitor='val_loss',
                               filepath='saved_models_checkpoint/weights_best_from_' + jobName + '.h5',
                               verbose=1, save_best_only=True)

# add tensorboard callback
# tb_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)
tb_callback = TensorBoard(log_dir=logdir)

# add Earlystop callback
earlystopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=10,
                              verbose=1, mode='auto', restore_best_weights=True)

# add ReduceLROnPlateau callback
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.75, patience=10, verbose=1)


def evaluate(model, testX, testY):
    # evaluate the model
    test_loss, test_acc = model.evaluate(testX, testY, verbose=2)

    # batch predict on the testing set
    predictions = model.predict(testX, batch_size=args.batch_size)

    # display pretty classification report ;
    class_report = classification_report(testY.argmax(axis=1), predictions.argmax(axis=1))

    acc = accuracy_score(testY.argmax(axis=1), predictions.argmax(axis=1))
    precision = precision_score(testY.argmax(axis=1), predictions.argmax(axis=1))
    conf_matrix = confusion_matrix(testY.argmax(axis=1), predictions.argmax(axis=1))
    roc = metrics.roc_auc_score(testY.argmax(axis=1), model.predict(testX)[:, 1])

    # create confusion matrix plot
    temp_name = "confusion-matrix.png"
    conf_matrix = confusion_matrix(testY.argmax(axis=1), predictions.argmax(axis=1))
    ax_cm = sns.heatmap(conf_matrix, annot=True, fmt='g')
    ax_cm.invert_xaxis()
    ax_cm.invert_yaxis()
    plt.ylabel('Actual')
    plt.xlabel('Predicted')
    plt.title("confusion-matrix-plots")
    plt.savefig(temp_name)
    mlflow.log_artifact(temp_name, "confusion-matrix-plots")
    try:
        os.remove(temp_name)
    except FileNotFoundError as e:
        print(f"{temp_name} file is not found")

    # create roc plot
    plot_file = "roc-auc-plot.png"
    probs = model.predict(testX)[:, 1]
    fpr, tpr, thresholds = roc_curve(testY.argmax(axis=1), probs)
    auc_k = auc(fpr, tpr)
    plt_roc, fig_roc, ax_roc = utils.create_roc_plot(fpr, tpr, 0.0)  # you can select ylim like 0.8 to zoom in
    fig_roc.savefig(plot_file)
    mlflow.log_artifact(plot_file, "roc-auc-plots")
    try:
        os.remove(plot_file)
    except FileNotFoundError as e:
        print(f"{temp_name} file is not found")

    # log metrics
    mlflow.log_metric("test_loss", test_loss)
    mlflow.log_metric("test_accuracy", test_acc)
    mlflow.log_metric("auc", auc_k)

    # print outputs
    print(conf_matrix)
    print(class_report)
    _logger.info("[TRAINING]:Accuracy Score =>", round(acc, 3))
    _logger.info("[TRAINING]:Precision      =>", round(precision, 3))
    _logger.info("[TRAINING]:ROC            =>", round(roc, 3))


def train_mlflow(run_name='TensorFlow-Keras_NailBiterAlerter', experiment_name='experiment_name', model_summary=False,
                 opt_name="Adam_decay"):
    """
    Method to run MLflow experiment
    :param experiment_name: custom naming for this experiment
    :param run_name: name of the run
    :param model_summary: print model summary; default is False
    :param opt_name: name of the optimizer to use
    :return: Keras optimizer

    """
    mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)
    mlflow.tensorflow.autolog(every_n_iter=1)
    mlflow.set_experiment(experiment_name=experiment_name)

    with mlflow.start_run(run_name=run_name) as run:
        logging.info(f'Start mlflow train - id = {run.info.run_id}')
        capture_message(f'Start mlflow train - id = {run.info.run_id}')
        run_id = mlflow.active_run().info.run_id
        mlflow.set_tag('entry_point', 'train')
        # mlflow.set_tag("dev version", "test_keras_scratch_1")

        # log parameters used in the command line args use in the model
        [mlflow.log_param(arg, getattr(args, arg)) for arg in vars(args)]
        mlflow.log_param("TF version", tf.__version__)
        mlflow.log_param("MLFlow version", mlflow.__version__)
        mlflow.log_param("dataset test_size", test_size)

        # log data augmentation dataset params
        mlflow.log_param("data_augm_rotation_range", rotation_range)
        mlflow.log_param("data_augm_zoom_range", zoom_range)
        mlflow.log_param("data_augm_width_shift_range", width_shift_range)
        mlflow.log_param("data_augm_height_shift_range", height_shift_range)
        mlflow.log_param("data_augm_shear_range", shear_range)
        mlflow.log_param("data_augm_horizontal_flip", horizontal_flip)
        mlflow.log_param("data_augm_fill_mode", fill_mode)

        # load the MobileNetV2 network, ensuring the head FC layer sets are left off
        baseModel = MobileNetV2(weights="imagenet", include_top=False,
                                input_tensor=Input(shape=(224, 224, 3)))

        # construct the head of the model that will be placed on top of the
        # the base model
        headModel = baseModel.output
        headModel = AveragePooling2D(pool_size=(args.psize, args.psize))(headModel)
        headModel = Flatten(name="flatten")(headModel)
        headModel = Dense(args.num_hidden_units, activation="relu")(headModel)
        headModel = Dropout(args.dropout)(headModel)
        headModel = Dense(2, activation="softmax")(headModel)

        # place the head FC model on top of the base model (this will become the actual model we will train)
        model = Model(inputs=baseModel.input, outputs=headModel)

        # model_concat.summary()

        # prune whole model (trial to prune whole model ; still some bugs to dig in later
        #  https://www.tensorflow.org/model_optimization/guide/pruning/comprehensive_guide.md
        # model = tfmot.sparsity.keras.prune_low_magnitude(model_concat)

        # model.summary()

        # loop over all layers in the base model and freeze them so they will
        # not be updated during the first training process
        for layer in baseModel.layers:
            layer.trainable = False

        # compile our model
        _logger.info("[TRAINING]:model compile")

        # call optimizer function
        opt = get_optimizer(opt_name)

        # compile the model
        model.compile(optimizer=opt, loss="binary_crossentropy",  # only 2 class nail-niting / no-nail-biting
                      metrics=["accuracy"])

        # get experiment id and run id
        runID = run.info.run_uuid
        experimentID = run.info.experiment_id
        _logger.info("[TRAINING]: MLflow Run with run_id {} and experiment_id {}".format(runID, experimentID))

        # fit the model
        # train the head of the network (bottom is frozen)
        _logger.info("[TRAINING]: model training")
        # history = model.fit(
        model.fit(
            aug.flow(trainX, trainY, batch_size=args.batch_size),
            steps_per_epoch=len(trainX) // args.batch_size,
            validation_data=(testX, testY),
            callbacks=[
                checkpointer,
                tb_callback,
                earlystopping,
                reduce_lr,
            ],
            validation_steps=len(testX) // args.batch_size,
            epochs=args.epochs)

        mlflow.log_param("optimizer", opt_name)
        mlflow.log_param("loss_function", "binary_crossentropy")

        # Evaluate model and plot Confusion matrix and ROC (embeded in evaluate function)
        evaluate(model, testX, testY)

        # fetch logged data
        _logger.info('[TRAINING]: fetching metrics to mlflow backend...')
        capture_message('[TRAINING]:  fetching metrics to mlflow backend...')

        # Save model as a TF model (use by mlflow)
        tf.keras.models.save_model(model, 'saved_models', save_format='tf')

        # save model with *.h5 format
        # tf.keras.models.save_model(model, 'saved_models_keras', save_format='h5')
        model.save('saved_models_h5/model.h5')

        # shortcut to tfjs conversion function
        # incompatible tfjs version so commented
        # tfjs.converters.save_keras_model(model, 'export_models/tfjs2')

        # # log model to Mlflow registry
        mlflow.tensorflow.log_model(tf_saved_model_dir='saved_models',
                                    tf_meta_graph_tags='serve',
                                    tf_signature_def_key='serving_default',
                                    artifact_path='tf_keras')

        # Save model to MLFlow Model Registry
        model_name = 'tf_keras_v1'
        # The default path where the MLflow autologging function stores the model
        artifact_path = "model"
        model_uri = "runs:/{run_id}/{artifact_path}".format(run_id=run_id, artifact_path=artifact_path)

        model_details = mlflow.register_model(model_uri=model_uri, name=model_name)

        # mlflow.log_artifact(f"{cfg_inf['mlflow_tracking'].get('mlflow_bucket')}_data.csv")

        # useful function to wait model is really registered
        wait_until_ready(model_details.name, model_details.version)

        _logger.info(f"[TRAINING]: new model name is: {model_details.name}")
        _logger.info(f"[TRAINING]: new model version is: {model_details.version}")

        # seting  MLFow registry stage model according to GIT version (CICD)
        # get git branch name
        git_branch = Repository('.').head.shorthand  # 'master'
        mlflow_client = mlflow.tracking.MlflowClient()
        # always place last dev model in Staging
        if git_branch != "master":
            # transition_newest_to_stage(mlflow_client, model_name=model_details.name, stage="Staging")
            stage = "Staging"
            mlflow_client.transition_model_version_stage(model_name,
                                                         version=model_details.version,
                                                         stage=stage)
            _logger.info(f"[TRAINING]: {model_name} model, with version number {model_details.version} has been put "
                         f"on Model registry "
                         f"stage: {stage} ")

        # TODO log datas source as artifact > add encryption first ???
        # mlflow.log_artifact(f"{cfg_inf['mlflow_tracking'].get('mlflow_bucket')}_data.csv")

        _logger.info("MLflow completed with run_id {} and experiment_id {}".format(runID, experimentID))
        _logger.info("[TRAINING]: Using TensorFlow Version={}".format(tf.__version__))
        _logger.info("[TRAINING]: Using MLflow Version={}".format(mlflow.__version__))

        _logger.info('[TRAINING]: finished...')
        capture_message('[TRAINING]: finished...')
        _logger.info("-" * 100)


if __name__ == '__main__':
    # TrainModel.run_training()

    experiment_name = 'keras_m2'
    run_name = 'Mobilenetv2_TL_m2'
    train_mlflow(run_name=run_name, experiment_name=experiment_name, model_summary=False)


