# import click
# import mlflow
# import mlflow.sklearn
# import mlflow.pyfunc
#
#
# from sklearn.ensemble import RandomForestRegressor
# from nailbiter.settings import DATA_DIR, LOGGING_CONFIGURATION_FILE  # type: ignore
# from infrastructure.pipeline import load
# # from nailbiter.domain.feature_engineering import features_offline, features_online
# from nailbiter.application.mlflow_utils import mlflow_log_pandas, mlflow_log_plotly
# from nailbiter.domain.forecast import cross_validate, plotly_predictions
# from nailbiter.infrastructure.config.config import config as cfg_inf
#
# import nailbiter.infrastructure.data_ingest as data_ingest
# import nailbiter.infrastructure.preprocessing as preprocessing
# import nailbiter.domain.descriptor_aggregate as descriptor_aggregate
# from nailbiter.application.mlflow_utils import mlflow_log_pandas
# import nailbiter.infrastructure.database as db_tools
# from sentry_sdk import capture_message, capture_event
# import yaml
# import pandas as pd
# import logging
# import logging.config
# with open(LOGGING_CONFIGURATION_FILE, 'r') as f:
#     logging.config.dictConfig(yaml.safe_load(f.read()))
#
# logging.basicConfig(level=logging.INFO)
# _logger = logging.getLogger(__name__)
#
#
# if os.getenv('DB_USER') is None:
#     os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'yotta-mlops
# else:
#     os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = '/secrets/key.json'
#
# @click.command(
#     help='Run the ingestion from psql.'
# )
# @click.option(
#     '--is_triggered',
#     type=click.BOOL,
#     default=data_ingest.is_ingest_triggered(),
#     help='override the ingestion from psql even if no new data.'
# )
# def run_pipeline(
#     is_triggered: bool,
#     # start_week: int,
# ) -> None:
#
#    # mlflow.set_tracking_uri(cfg_inf['mlflow_tracking'].get('uri'))
#     mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)
#
#     with mlflow.start_run(run_name='run_pipeline') as run:
#         logging.info(f"Start mlflow run {run.data.tags['mlflow.project.entryPoint']} - id = {run.info.run_id}")
#         mlflow.set_tag('entry_point', 'run_pipeline')
#         mlflow.log_params(
#             {
#                 'is_trigered': is_triggered,
#             }
#         )
#
#         # Load
#         logging.info(f'Load data...')
#         mlflow.log_params({'is_ingest_triggered': data_ingest.is_ingest_triggered()})
#
#         if is_triggered:
#             _logger.info("Running cleaning pipeline")
#             capture_message("Running cleaning pipeline")
#
#             data_ingest.ingest_all()
#             # data_ingest.ingest_ref()
#             descriptor_aggregate.aggregate_all()
#
#             data_raw = pd.read_sql_table('raw_ads_apartments', db_tools.engine)
#             _logger.info("Saving data raw to mlflow backend")
#             capture_message("Saving data raw to mlflow backend")
#             mlflow_log_pandas(data_raw, 'data_raw', 'data_raw.csv')
#
#             data_cleaned = preprocessing.preprocessing_ads()
#             _logger.info("Saving data cleaned to mlflow backend")
#             capture_message("Saving data cleaned to mlflow backend")
#             mlflow_log_pandas(data_cleaned, 'data_cleaned', 'data_cleaned.csv')
#
#         else:
#             _logger.info("No new file to clean")
#             capture_message("No new file to clean")
#
#
#
#         # Train
#         logging.info(f'Train model...')
#         model.fit(x_train, y_train)
#         mlflow.sklearn.log_model(
#             sk_model=model.single_estimator,
#             artifact_path='simple_model',
#         )
#         mlflow.pyfunc.log_model(
#             python_model=model,
#             artifact_path='lasso_model',
#             code_path=['nailbiter'],
#             conda_env={
#                 'channels': ['defaults', 'conda-forge'],
#                 'dependencies': [
#                     'python=3.7.6',
#                     'mlflow=1.8.0',
#                     'numpy=1.17.4',
#                     'scikit-learn=0.21.3',
#                     'cloudpickle=1.3.0'
#                 ],
#                 'name': 'multi-model-env'
#             }
#         )
#         logging.info(f'mlflow.pyfunc.log_model:\n{model}')
#
#         # Future
#         logging.info(f'Build future...')
#         past = etl(DATA_DIR, next_week - lag_in_week, next_week - 1)
#         x_pred = span_future(past['order_date'].max())
#         x_pred = features_online(x_pred, past, degree=degree, lag_in_week=lag_in_week)
#         mlflow_log_pandas(x_pred, 'prediction_set', 'x_pred.csv')
#         x_pred = x_pred.set_index('order_date')
#         mlflow_log_pandas(x_pred, 'prediction_set', 'x_pred.json')
#
#         # Predict
#         logging.info(f'Predict future...')
#         y_pred = model.predict(None, x_pred)
#         fig = plotly_predictions(y_pred)
#         mlflow_log_plotly(fig, 'plots', 'predictions.html')
#         mlflow_log_pandas(y_pred.reset_index(), 'predictions', 'y_pred.csv')
#
#
# if __name__ == '__main__':  # pragma: no cover
#     run_pipeline()
