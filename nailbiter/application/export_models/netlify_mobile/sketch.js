// Classifier Variable
let classifier;
// Model URL
let imageModelURL = 'https://demo-poc-ml-nail-biter-alerter.s3.eu-west-3.amazonaws.com/model.json';
let frameCount = 0;
let startTime = performance.now();
let fps = 0;

// Notify audio
//let notifyAudio = new Audio('short-success.mp3');
let notifyAudio = new Audio('beep-beep.mp3');

// Video
let video;
let flippedVideo;
// To store the classification
let label = "";
let confidence = "";
let labelstring = "";

//function modelReady() {
//   console.log.('Model is ready!!!');
//}

// Load the model first
function preload() {
  classifier = ml5.imageClassifier(imageModelURL);
}

function setup() {
  createCanvas(320, 285);
  //background(0);
  // Create the video
  video = createCapture(VIDEO);
  video.size(320, 235);
  video.hide();

  flippedVideo = ml5.flipImage(video);
  // Start classifying
  classifyVideo();
}

function draw() {
  // background(125);
  background(0);
  // Draw the video
  image(flippedVideo, 0, 0);

  // Draw the label
  fill(240);
  textSize(14);
  textAlign(CENTER);
  text('Label: ' + label, (width / 2) - 80, height - 30);
  text('Confidence: '+ confidence, (width / 2) + 80, height - 30);
  text('Mem. Usage: ' + Math.round(performance.memory.usedJSHeapSize / 1000000) + ' MB', (width / 2) - 80 , height - 8);
//  text('CPU Time: ' + (performance.now() / 1000).toFixed(2) + ' sec', (width / 2) + 100, height -8);

  // Calculate and display FPS
  frameCount++;
  if (performance.now() - startTime >= 1000) {
    fps = frameCount;
    frameCount = 0;
    startTime = performance.now();
  }
  text('FPS: ' + fps, (width / 2) + 80 , height - 8);
}

// Get a prediction for the current video frame
function classifyVideo() {
  flippedVideo = ml5.flipImage(video);
  classifier.classify(flippedVideo, gotResult);
  flippedVideo.remove();
}

// When we get a result
function gotResult(error, results) {
  // If there is an error
  if (error) {
    console.error(error);
    return;
  }
  // The results are in an array ordered by confidence.
  // console.log(results[0]);
  label = results[0].label;
  confidence = nf(results[0].confidence, 0, 2);
  if (label === "no-nail-biting"){
    document.body.style.backgroundColor = "#00b712";
    document.body.style.backgroundImage = "linear-gradient(315deg, #00b712 0%, #5aff15 74%)";
  }
  if (label === "nail-biting"){
    document.body.style.backgroundColor = "#ff416c";
    document.body.style.backgroundImage = "linear-gradient(to right, #ff416c, #ff4b2b)";

//    Play sound trigger
    notifyAudio.play();
  }
  // Classifiy again!
  classifyVideo();
}
