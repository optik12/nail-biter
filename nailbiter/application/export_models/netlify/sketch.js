// Classifier Variable
let classifier;
// Model URL
let imageModelURL = 'https://demo-poc-ml-nail-biter-alerter.s3.eu-west-3.amazonaws.com/model.json';
let frameCount = 0;
let startTime = performance.now();
let fps = 0;

// Notify audio
let notifyAudio = new Audio('beep-beep.mp3');
let notifyCounter = 0;
let notifyTimer = 0;
let notifyInterval = 200; // 200 millisecondes par défaut

// Video
let video;
let flippedVideo;
// To store the classification
let label = "";
let confidence = "";
let labelstring = "";

// Graph variables
let history = [];
let maxHistoryLength = 30 * 60; // 30 minutes, avec une mise à jour par seconde
let graphHeight = 50; // Réduire la hauteur du graphique de moitié

// Memory usage variables
let memUsageHistory = [];
let memUsageWindowSize = 60; // Utilisez une fenêtre d'une seconde si vous êtes à 60 FPS

function preload() {
  classifier = ml5.imageClassifier(imageModelURL);
}

function setup() {
  let container = createDiv();
  container.style("width", "480px");
  container.style("margin", "0 auto");

  createCanvas(480, 380 + graphHeight).parent(container); // Ajoutez la hauteur du graphique

  video = createCapture(VIDEO);
  video.size(480, 328);
  video.hide();
  flippedVideo = ml5.flipImage(video);
  classifyVideo();

  // Créer un conteneur pour les éléments input et bouton
  let controlsContainer = createDiv();
  controlsContainer.style("text-align", "center");
  controlsContainer.style("margin-top", "10px");
  controlsContainer.parent(container);

  // Créer un bouton pour régler la durée de l'intervalle de notification
  let intervalInput = createInput(notifyInterval.toString(), "number");
  let intervalButton = createButton("Set Interval");

  intervalInput.parent(controlsContainer);
  intervalButton.parent(controlsContainer);

  // Définir la largeur de l'input intervalInput à 50%
  intervalInput.style("width", "50%");

  intervalButton.mousePressed(() => {
    notifyInterval = Number(intervalInput.value());
    intervalInput.value(notifyInterval.toString());
  });
}

function draw() {
  background(0);
  image(flippedVideo, 0, 0);

  fill(240);
  textSize(16);
  textAlign(CENTER);
  text('Label: ' + label, (width / 2) - 100, height - 30 - graphHeight);
  text('Confidence: '+ confidence, (width / 2) + 100, height - 30 - graphHeight);

  // Add the current memory usage to history
  memUsageHistory.push(performance.memory.usedJSHeapSize / 1000000);

  // Remove the oldest memory usage if we've exceeded the window size
  if (memUsageHistory.length > memUsageWindowSize) {
    memUsageHistory.shift();
  }

  // Compute the average memory usage
  let averageMemUsage = memUsageHistory.reduce((a, b) => a + b) / memUsageHistory.length;

  // Display the average memory usage
  text('Mem. Usage: ' + Math.round(averageMemUsage) + ' MB', (width / 2) - 100 , height - 8 - graphHeight);

  frameCount++;
  if (performance.now() - startTime >= 1000) {
    fps = frameCount;
    frameCount = 0;
    startTime = performance.now();
  }
  text('FPS: ' + fps, (width / 2) + 100 , height - 8 - graphHeight);

  drawGraph(); // Ajoutez cette ligne
}

function classifyVideo() {
  flippedVideo = ml5.flipImage(video);
  classifier.classify(flippedVideo, gotResult);
  flippedVideo.remove();
}

function gotResult(error, results) {
  if (error) {
    console.error(error);
    return;
  }
  label = results[0].label;
  confidence = nf(results[0].confidence, 0, 2);
  if (label === "no-nail-biting"){
    document.body.style.backgroundColor = "#00b712";
    document.body.style.backgroundImage = "linear-gradient(315deg, #00b712 0%, #5aff15 74%)";
    notifyCounter = 0;
  }
  if (label === "nail-biting") {
    document.body.style.backgroundColor = "#ff416c";
    document.body.style.backgroundImage = "linear-gradient(to right, #ff416c, #ff4b2b)";
    notifyCounter++;
    if (notifyCounter === 1) {
      notifyTimer = performance.now();
    } else if (notifyCounter > 1 && performance.now() - notifyTimer >= notifyInterval) {
      notifyAudio.play();
      notifyCounter = 0;
    }
  }
  classifyVideo();
  updateHistory(); // Ajoutez cette ligne
}

function updateHistory() {
  history.push(label === "nail-biting" ? 1 : 0);
  if (history.length > maxHistoryLength) {
    history.shift();
  }
}

function drawGraph() {
  let graphWidth = width;
  let barWidth = graphWidth / maxHistoryLength;
  let x = 0;

  // Ajouter un fond gris dans la zone du graphique
  fill(200);
  rect(0, height - graphHeight, graphWidth, graphHeight);

  for (let value of history) {
    let barHeight = map(value, 0, 1, 0, graphHeight - 4); // Soustraire la largeur du contour (2 * 2.5 pixels) de la hauteur maximale
    let y = height - barHeight;

    // Ajouter un contour noir à l'intérieur de la zone grise
    strokeWeight(4); // Largeur du contour de 5 pixels
    stroke(0); // Couleur du contour noir
    noFill();
    rect(2, height - graphHeight + 2, graphWidth - 4, graphHeight - 4); // Modifier les coordonnées en conséquence

    if (value) {
      fill(255, 0, 0); // Barres rouges
    } else {
      fill(255); // Barres blanches
    }
    noStroke(); // Ajout de cette ligne pour supprimer les contours noirs
    rect(x, y, barWidth, barHeight);
    x += barWidth;
  }
}

