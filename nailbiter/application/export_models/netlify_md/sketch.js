// Classifier Variable
let classifier;
// Model URL
let imageModelURL = 'https://demo-poc-ml-nail-biter-alerter.s3.eu-west-3.amazonaws.com/model.json';
let notifyAudio = document.getElementById("notifyAudio");

// Video
let video;
let flippedVideo;
// To store the classification
let label = "";
let confidence = "";
let labelstring = "";

//function modelReady() {
//   console.log.('Model is ready!!!');
//}

// Load the model first
function preload() {
  classifier = ml5.imageClassifier(imageModelURL);
}

function setup() {
  createCanvas(480, 380);
  //background(0);
  // Create the video
  video = createCapture(VIDEO);
  video.size(480, 328);
  video.hide();

  flippedVideo = ml5.flipImage(video);
  // Start classifying
  classifyVideo();
}

function draw() {
  // background(125);
  background(0);
  // Draw the video
  image(flippedVideo, 0, 0);

  // Draw the label
  fill(240);
  textSize(16);
  textAlign(CENTER);
  text('label: ' + label, width / 2, height - 30);
  text('confidence: '+ confidence, width / 2, height - 8);
}

// Get a prediction for the current video frame
function classifyVideo() {
  flippedVideo = ml5.flipImage(video);
  classifier.classify(flippedVideo, gotResult);
  flippedVideo.remove();
}

// When we get a result
function gotResult(error, results) {
  // If there is an error
  if (error) {
    console.error(error);
    return;
  }
  // The results are in an array ordered by confidence.
  // console.log(results[0]);
  label = results[0].label;
  confidence = nf(results[0].confidence, 0, 2);
  if (label === "no-nail-biting"){
    document.body.style.backgroundColor = "green";
  }
  if (label === "nail-biting"){
    document.body.style.backgroundColor = "red";
//    Play sound trigger not working sound without user interaction: cf https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
//    notifyAudio.play();
  }
  // Classifiy again!
  classifyVideo();
}
