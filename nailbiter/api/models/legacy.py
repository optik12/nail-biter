from flask_restx import fields
from nailbiter.api.baseapi import BaseApi

app_ns_sentry = BaseApi.get_base_api().namespace(path='/v1', name='Sentry', description='sentry trigger errors')

# sentry namespace model
sentry = app_ns_sentry.model('health_service', {'sentry': fields.String(description="sentry trigger errors")})


api_ns_health = BaseApi.get_base_api().namespace(path='/v1', name='Health service check', description='health service')

# health namespace model
health = api_ns_health.model('health_service', {'health': fields.String(example="API UP")})
