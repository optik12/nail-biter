import os
from flask import jsonify, request
from flask_restx import Resource

from nailbiter.api.models.legacy import health
from nailbiter.api.baseapi import BaseApi
from nailbiter.api.mlflowserving.settings import MLFLOW_TRACKING_URI
from nailbiter.api.config import get_logger

import logging

_logger = get_logger(logger_name=__name__)

if os.getenv('DB_USER') is None:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'mlops-jmp-b1d16aa1c3c2.json'
    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = '/secrets/key.json'
else:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = '/secrets/key.json'


# #  API HEALTH CHECK
api_ns_health = BaseApi.get_base_api().namespace(path='/v1', name='Health service check', description='health service')


@api_ns_health.route('/health', methods=["GET"])
class Health(Resource):
    @api_ns_health.doc(responses={400: 'Bad request'})
    @api_ns_health.response(200, 'Success', health)
    def get(self):
        """
        Simple health service which respond API UP to an /health request

        :return {"health": 'API UP'}
        """
        if request.method == 'GET':
            _logger.info('health status OK')
            response = {"health": 'API UP'}
            return jsonify(response)


@api_ns_health.errorhandler(ValueError)
@api_ns_health.errorhandler(KeyError)
def handle_custom_exception_desc(error):
    """
    Return a custom message and 400 status code
    """
    return {'message': str(error)}, 417


# API Sentry trigger log error: ceck logging to Sentry
app_ns_sentry = BaseApi.get_base_api().namespace(path='/v1', name='Sentry', description='sentry trigger errors')


@app_ns_sentry.route('/debug-sentry', methods=["GET"])
class Sentry(Resource):
    @app_ns_sentry.doc(responses={400: 'Bad request'})
    def get(self):
        return 1 / 0


# MLFLOW URI
app_ns_mlflow = BaseApi.get_base_api().namespace(path='/v1', name='MLFLOW url server', description='mlflow url stage')


@app_ns_mlflow.route("/mlflow_url")
class MlflowUri(Resource):
    @app_ns_mlflow.doc(responses={400: 'Bad request'})
    @app_ns_mlflow.doc(responses={500: 'internal error'})
    @app_ns_mlflow.response(200, 'Success')
    def get(self):
        return {"mlflow url": MLFLOW_TRACKING_URI}
