import os
from nailbiter.api.app import create_flask_app
from nailbiter.api.config import ProductionConfig

flask_app = create_flask_app(
    config_object=ProductionConfig)

if os.getenv('DB_USER') is None:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'mlops-jmp-b1d16aa1c3c2.json'
else:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = '/secrets/key.json'

if __name__ == '__main__':
    flask_app.run()
