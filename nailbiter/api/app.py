"""The app module, containing the app factory function."""
import logging.config

from flask import Flask
from nailbiter.api.lb import lb_bp
from nailbiter.api.baseapi import BaseApi
import nailbiter.api.baseapi as baseapi

_logger = logging.getLogger(__name__)


def create_flask_app(*, config_object) -> Flask:
    """An application factory, as explained here:
    http://flask.pocoo.org/docs/patterns/appfactories/.
    """

    flask_app = Flask(__name__.split('.')[0])
    flask_app.config.from_object(config_object)

    register_endpoints(flask_app)
    init(flask_app)
    register_after_request(flask_app)
    register_extensions(flask_app)
    register_blueprint(flask_app)
    register_errorhandlers(flask_app)

    _logger.info("site map : {}".format(flask_app.url_map))

    return flask_app


def register_endpoints(flask_app):
    """Register Flask extensions."""
    # noinspection PyUnresolvedReferences
    import nailbiter.api.server


def register_extensions(flask_app):
    """Register Flask extensions."""


def register_blueprint(flask_app):
    """Register Flask blueprint."""
    flask_app.register_blueprint(lb_bp, url_prefix='')
    flask_app.register_blueprint(BaseApi.get_base_api_bp(), url_prefix='')
    _logger.debug('application blueprint created')


def register_errorhandlers(flask_app):
    """Register Flask error handler."""


def init(flask_app):
    """Register Flask before request handler."""
    # PricePredictor.init()
    BaseApi.init()


def register_after_request(flask_app):
    """Register Flask after request handler."""
    flask_app.after_request(baseapi.add_headers)
