import os

MODEL_NAME = os.environ.get("MODEL_NAME", "kerasmodel")
STAGE = os.environ.get("STAGE", "Production")
# MLFLOW_TRACKING_URI = os.environ.get("MLFLOW_TRACKING_URI", "http://0.0.0.0:5000")
# K8s IP Endpoint
# TODO replace with Gitlab cICD
MLFLOW_TRACKING_URI = os.environ.get("MLFLOW_TRACKING_URI", "http://34.77.223.218:5000/")
