from flask import Blueprint, url_for
from flask_restx import Api

import os


class BaseApi:
    """
    Classe de gestion des api
    """

    _base_api_bp = None
    _base_api = None

    @classmethod
    def get_base_api_bp(cls):
        """
        Instanciation du blueprint principal des api de l'application
        :return: blueprint
        """
        if cls._base_api_bp is None:
            cls._base_api_bp = Blueprint('base_api', __name__)
        return cls._base_api_bp

    @classmethod
    def get_base_api(cls):
        """
        Instanciation de l'api principal
        :return: Api
        """
        if cls._base_api is None:
            cls._base_api = Api(BaseApi.get_base_api_bp(), )
        return cls._base_api

    @classmethod
    def init(cls):
        """
        init de l'api
        :return: Api
        """
        if os.environ.get('HTTPS') == 'YES':
            @property
            def specs_url(self):
                """Monkey patch for HTTPS"""
                return url_for(self.endpoint('specs'), _external=True, _scheme='https')

            Api.specs_url = specs_url


def add_headers(resp):
    """
    add default headers for http responses
    :return: resp
    """

    resp.headers['expires'] = 0
    resp.headers['pragma'] = 'no-cache'
    resp.headers['X-Frame-Options'] = 'deny'
    resp.headers['X-XSS-Protection'] = '1; mode=block'
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'

    return resp
