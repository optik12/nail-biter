from flask import Blueprint, Response

lb_bp = Blueprint('lb', __name__, url_prefix='/')


@lb_bp.route('/lb.jsp')
def lb():
    response = Response("""
    """)
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"  # HTTP 1.1.
    response.headers["Pragma"] = "no-cache"  # HTTP 1.0.
    response.headers["Expires"] = "0"  # Proxies.

    return response
