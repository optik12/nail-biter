import os
import pytest

from nailbiter.api.app import create_flask_app
from nailbiter.api.config import TestingConfig

# os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'yotta-mlops-2ef5ce39fe63.json'

#
# @pytest.fixture
# def app():
#     app = create_flask_app(config_object=TestingConfig)
#
#     with app.app_context():
#         yield app
#
#
# @pytest.fixture
# def flask_test_client(app):
#     with app.test_client() as test_client:
#         yield test_client
