from setuptools import setup, find_packages

setup(
    name='nailbiter',
    url='https://gitlab.com/optik12/nail-biter',
    author='optik12@gmail.com',
    description='Original code base for MLOps anti nail-biter',
    packages=find_packages(),
    include_package_data=True
)
